import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex';
import vuetify from './plugins/vuetify';
import * as fb from 'firebase'
import store from './store'

Vue.use(Vuex);

Vue.config.productionTip = false

new Vue({
  vuetify,
  store,
  render: h => h(App),
  created () {
    fb.initializeApp ({
      apiKey: "AIzaSyDkWDXqHXXVT3Yg_B_WF3Eq5hrm36ez_kw",
          authDomain: "mail-iq-test.firebaseapp.com",
        databaseURL: "https://mail-iq-test.firebaseio.com",
        projectId: "mail-iq-test",
        storageBucket: "mail-iq-test.appspot.com",
        messagingSenderId: "892210491088",
        appId: "1:892210491088:web:90a9d4a7eac29ecef078d5"
    })
    this.$store.dispatch('fetchPolls')
    this.$store.dispatch('fetchUsers')
  },
}).$mount('#app')
