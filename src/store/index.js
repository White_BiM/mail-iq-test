import Vue from "vue"
import Vuex from 'vuex'
import polls from './polls'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        polls
    }

})
