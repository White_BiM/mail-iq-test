import * as fb from "firebase";
import Swal from 'sweetalert2';

export default {
  state: {
    polls: [],
    users: []
  },
  mutations: {
    loadPolls(state, payload) {
      const newPolls = []
      Object.keys(payload).forEach(key => {
        const poll = payload[key];
        newPolls.push(poll);
      });
      state.polls = newPolls
    },
    loadUsers(state, payload) {
      const newUsers = []
      Object.keys(payload).forEach(key => {
        const user = payload[key];
        newUsers.push(user);
      });
      state.users = newUsers
    }
  },

  actions: {
    async fetchPolls({ commit }) {
      try {
        const pollsVal = await fb
          .database()
          .ref("/polls")
          .once("value");
        const polls = pollsVal.val();
        console.log(polls);
        commit("loadPolls", polls);
      } catch (error) {
        commit("setError", error.message);
        throw error;
      }
    },
    async fetchUsers({ commit }) {
      try {
        const usersVal = await fb
          .database()
          .ref("/users")
          .once("value");
        const users = usersVal.val();
        console.log(users);
        commit("loadUsers", users);
      } catch (error) {
        commit("setError", error.message);
        throw error;
      }
    },
    async sendAnswers({ commit }, { answers, userData }) {
      try {
        userData.id = null;
        answers.id = null;
        const user = await fb
          .database()
          .ref("users/")
          .push(userData);
        const userId = (userData.id = user.key);
        await fb
          .database()
          .ref("users/")
          .child(user.key)
          .update({
            id: userId
          });
        const answersData = { userId, ...answers };
        const answer = await fb
          .database()
          .ref("polls/")
          .push(answersData);
        const answerId = (answers.id = answer.key);
        await fb
          .database()
          .ref("polls/")
          .child(answer.key)
          .update({
            id: answerId
          });
        console.log({
          answers,
          userData
        });
        this.dispatch("fetchPolls");
        this.dispatch("fetchUsers");
        if (answers[1] === '1' && answers[2] === '2' && answers[3] === '1') {
          Swal.fire({
            title: 'Ваш ответ принят!',
            text: 'Ошибок нет',
            icon: 'success',
          })
        } else {
          Swal.fire({
            title: 'Ваш ответ принят!',
            text: 'Но есть ошибки :(',
            icon: 'error',
          })
        }
      } catch (error) {
        commit("setError", error.message);
        throw error;
      }
    }
  },

  getters: {
    polls(state) {
      return state.polls;
    },
    users(state) {
      return state.users;
    }
  }
};
